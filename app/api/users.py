from app.api import bp
from flask import jsonify, request,g
from app.models import User
from flask import url_for
from app import db
from app.api.errors import bad_request
from app.api.auth import token_auth

@bp.route('/users/<int:id>', methods=['GET'])
@token_auth.login_required
def get_user(id):
    if g.current_user.role.access_user != True:
        return bad_request("You don't have access to this module!")
    return jsonify(User.query.get_or_404(id).to_dict())

@bp.route('/users', methods=['GET'])
@token_auth.login_required
def get_users():
    if g.current_user.role.access_user != True:
        return bad_request("You don't have access to this module!")
    page = request.args.get('page', 1, type=int)
    per_page = min(request.args.get('per_page', 10, type=int), 100)
    data = User.to_collection_dict(User.query, page, per_page, 'api.get_users')
    return jsonify(data)
