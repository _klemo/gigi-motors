from app.api import bp
from flask import jsonify, request, g
from app.models import *
from flask import url_for
from app import db
from flask_uploads import UploadSet, configure_uploads, IMAGES
from app.api.errors import bad_request
from app.api.auth import token_auth
import base64
import os
from datetime import datetime
photos = UploadSet('photos', IMAGES)

@bp.route('/inventory/<int:id>', methods=['GET'])
@token_auth.login_required
def get_inventory(id):
    if g.current_user.role.access_inventory != True:
        return bad_request("You don't have access to this module!")
    return jsonify(Inventory.query.get_or_404(id).to_dict())

@bp.route('/inventory', methods=['GET'])
@token_auth.login_required
def get_inventories():
    if g.current_user.role.access_inventory != True:
        return bad_request("You don't have access to this module!")
    page = request.args.get('page', 1, type=int)
    per_page = min(request.args.get('per_page', 10, type=int), 100)
    data = Inventory.to_collection_dict(Inventory.query,Inventory, page, per_page, 'api.get_inventories')
    return jsonify(data)

@bp.route('/create_inventory', methods=['POST'])
@token_auth.login_required
def create_inventory():
    if g.current_user.role.permissions.create_inventory != True:
        return bad_request("You don't have access to this module!")
    data = request.form or request.get_json() or {}
    if 'model' not in data or 'color' not in data or 'plate' not in data  or 'body' not in data or 'transmission' not in data or 'fuel' not in data or 'engine' not in data or 'mileage' not in data or 'price' not in data or 'state' not in data:
        return bad_request('some fields missing')
    inventory = Inventory()
    inventory.from_dict(data)
    files = request.files or None
    if files is not None:
        image=files['image']
        if image is not None:
            image = photos.save(request.files['image'])
            setattr(inventory, 'image', image)
    db.session.add(inventory)
    db.session.commit()
    response = jsonify(inventory.to_dict())
    response.status_code = 201
    response.headers['Location'] = url_for('api.get_inventory', id=inventory.id)
    return response

@bp.route('/_get_year', methods=['GET'])
@token_auth.login_required
def _get_year():
    data = {
        'years': [{'value': row.year,'year': row.year} for row in Year.query.all()]
        }
    return jsonify(data)

@bp.route('/_get_make', methods=['GET'])
@token_auth.login_required
def _get_make():
    data = {
        'makes': [{'value': row.make,'make': row.make} for row in Vehicle.query.group_by(Vehicle.make)]
        }
    return jsonify(data)

@bp.route('/_get_model', methods=['GET'])
@token_auth.login_required
def _get_model():
    make = request.args.get('make', '01', type=str)
    data = {
        'models': [{'value': row.id,'make': row.model} for row in Vehicle.query.filter_by(make=make).group_by(Vehicle.model)]
        }
    return jsonify(data)

@bp.route('/enquiry/<int:id>', methods=['GET'])
@token_auth.login_required
def get_enquiry(id):
    if g.current_user.role.access_inventory != True:
        return bad_request("You don't have access to this module!")
    return jsonify(Contact.query.get_or_404(id).to_enquiry_dict())

@bp.route('/enquiry', methods=['GET'])
@token_auth.login_required
def get_enquiries():
    if g.current_user.role.access_inventory != True:
        return bad_request("You don't have access to this module!")
    page = request.args.get('page', 1, type=int)
    per_page = min(request.args.get('per_page', 10, type=int), 100)
    data = Contact.to_collection_dict(Contact.query,Contact, page, per_page, 'api.get_enquiries')
    return jsonify(data)

@bp.route('/create_enquiry', methods=['POST'])
@token_auth.login_required
def create_enquiry():
    if g.current_user.role.permissions.create_inventory != True:
        return bad_request("You don't have access to this module!")
    data = request.form or {}
    if 'model' not in data or 'budget' not in data or 'client' not in data or 'year' not in data:
        return bad_request('some fields missing')
    enquiry = Enquiry()
    enquiry.from_enquiry_dict(data)
    setattr(enquiry, 'author', g.current_user)
    db.session.add(enquiry)
    db.session.commit()
    response = jsonify(enquiry.to_enquiry_dict())
    response.status_code = 201
    response.headers['Location'] = url_for('api.get_enquiry', id=enquiry.id)
    return response

@bp.route('/_get_client', methods=['GET'])
@token_auth.login_required
def _get_client():
    data = {
        'clients': [{'value': row.id,'name': row.client_name} for row in Client.query.order_by(Client.created_at.desc()).all()]
        }
    return jsonify(data)

@bp.route('/search-inventory', methods=['GET'])
@token_auth.login_required
def search_inventory():
    if g.current_user.role.access_inventory != True:
        return bad_request("You don't have access to this module!")
    page = request.args.get('page', 1, type=int)
    search = request.args.get('search', 'none', type=str)
    per_page = min(request.args.get('per_page', 10, type=int), 100)
    data = Inventory.search_inventory_filters(Inventory,page,search, per_page, 'api.search_inventory')
    return jsonify(data)

@bp.route('/upload', methods=['POST'])
@token_auth.login_required
def upload():
    image = request.get_json()['image']
    header, encoded = image.split(",", 1)
    data = base64.b64decode(encoded)
    file_name=str(datetime.utcnow()).replace('-', '').replace(' ', '') + ".png"
    if not os.path.exists('app/static/uploads'):
        os.mkdir('app/static/uploads')
    uploaded_file_path = os.path.join('app/static/uploads',file_name)
    with open(uploaded_file_path, "wb") as f:
        f.write(data)
    response = jsonify({ 'name': file_name, 'image': url_for('static',filename='uploads/'+file_name) })
    response.status_code = 201
    response.headers['Location'] = url_for('static',filename='uploads/'+file_name)
    return response
