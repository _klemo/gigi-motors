# uncompyle6 version 3.2.3
# Python bytecode 2.7 (62211)
# Decompiled from: Python 2.7.15rc1 (default, Apr 15 2018, 21:51:34)
# [GCC 7.3.0]
# Embedded file name: /run/user/1000/gvfs/smb-share:server=dibro-elite.local,share=pycharmprojects/lesho-construction/app/main/routes.py
# Compiled at: 2018-09-11 17:46:42
from datetime import datetime
from flask import render_template, flash, redirect, abort, url_for, request, g, jsonify, current_app
from flask_login import current_user, login_required, logout_user
from flask_uploads import UploadSet, configure_uploads, IMAGES
from app import db
from flask import Blueprint
from app.models import *
from app.main.forms import *
from xkcdpass import xkcd_password as xp
import os, random, string
from app.auth.email import send_user_added_email
from sqlalchemy import or_
main = Blueprint('main', __name__)
photos = UploadSet('photos', IMAGES)


@main.before_app_request
def before_request():
    if current_user.is_authenticated:
        if current_user.status == False:
            logout_user()
            flash('You are not activated please contact the administrator','danger')
            return redirect(url_for('auth.login'))
        current_user.last_seen = datetime.utcnow()
        db.session.commit()
    g.search_form = SearchForm()
    g.company = Company.query.first()
    g.images = Theme.query.first()
    car=Vehicle.query.first()
    if car is None:
        file_name = 'cars.csv'
        data = Vehicle.Load_Vehicle_Data(file_name)
        for i in data:
            record = Vehicle(**{
                'make' : i[0],
                'model' : i[1]
                })
            db.session.add(record)
        db.session.commit()
    year=Year.query.first()
    if year is None:
        file_name = 'years.csv'
        data = Year.Load_Year_Data(file_name)
        for i in data:
            record = Year(**{
                'year' : i[1]
            })
            db.session.add(record)
        db.session.commit()
@main.route('/add_property_features/<id>', methods=['GET', 'POST'])
@login_required
def add_property_features(id):
    if current_user.role.permissions.create_property != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    if request.method == 'POST':
        property = Property.query.filter_by(id=id).first_or_404()
        feature = PropertyFeature(feature_icon=request.form['feature_icon'], feature_name=request.form['feature_name'],
                                  property=property)
        db.session.add(feature)
        db.session.commit()
        return jsonify({'status': 1, 'message': 'feature edited successfully'})
    return jsonify(status=0)


@main.route('/', methods=['GET', 'POST'])
@login_required
def index():
    inventory_count = Inventory.query.count()
    reserved_count = Inventory.query.filter_by(state='reserved').count()
    sold_count =Inventory.query.filter_by(state='sold').count()
    available_count =Inventory.query.filter_by(state='available').count()
    count_all = {'inventory': inventory_count,'reserved': reserved_count,'sold': sold_count,'available': available_count}
    inventory = Inventory.query.limit(5)
    reserved = Inventory.query.filter_by(state='reserved').limit(5)
    sold = Inventory.query.filter_by(state='sold').limit(5)
    available = Inventory.query.filter_by(state='available').limit(5)
    tables = {'inventorylist': inventory,'reservedlist': reserved,'soldlist': sold,'availablelist': available}
    return render_template('admin/index.html', title='Dashboard',tables=tables, count_all=count_all)


#admin functions
@main.route('/users', methods=['GET', 'POST'])
@login_required
def users():
    if current_user.role.access_user != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    form = RegistrationForm()
    form.role.choices = [(row.id, row.role) for row in Role.query.order_by(Role.created_at.desc()).all()]
    if form.validate_on_submit():
        role = Role.query.filter_by(id=form.role.data).first_or_404()
        user = User(username=form.username.data, phone_number=form.phone_number.data, role=role, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('User added successfully!', 'success')
        return redirect(url_for('main.users'))
    users = User.query.order_by(User.last_seen.desc()).all()
    return render_template('admin/users.html', form=form, users=users)


@main.route('/edit_user/<id>', methods=['GET', 'POST'])
@login_required
def edit_user(id):
    if current_user.role.permissions.write_user != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    user = User.query.filter_by(id=id).first_or_404()
    form = EditRegistrationForm(obj=user)
    form.role.choices = [(row.id, row.role) for row in Role.query.order_by(Role.created_at.desc()).all()]
    if form.validate_on_submit():
        role = Role.query.filter_by(id=form.role.data).first_or_404()
        user.username = form.username.data
        user.role = role
        user.phone_number = form.phone_number.data
        db.session.commit()
        flash('User edited successfully', 'success')
        return redirect(url_for('main.users'))
    return render_template('admin/edit_user.html', form=form, title='Edit User')


@main.route('/add_role', methods=['GET', 'POST'])
@login_required
def add_role():
    if request.method == 'POST':
        role = Role(role=request.form['role'])
        permission = Permission(role=role)
        db.session.add(role)
        db.session.commit()
    return jsonify({'status': 1})

#edit role
@main.route('/edit_role/<id>', methods=['GET', 'POST'])
@login_required
def edit_role(id):
    role = Role.query.filter_by(id=id).first_or_404()
    if request.method == 'POST':
        role.role = request.form['role']
        db.session.commit()
        return jsonify({'status': 1})
    elif request.method == 'GET':
        data={'role':role.role, 'id':role.id}
        return jsonify({'status': 1, 'data':data})


@main.route('/view_user/<id>')
@login_required
def view_user(id):
    if current_user.role.permissions.read_user != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    user = User.query.filter_by(id=id).first_or_404()
    return render_template('admin/view_user.html', title='view user', user=user)

@main.route('/activate/<user_id>')
@login_required
def activate(user_id):
    if current_user.role.permissions.write_user != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    user = User.query.filter_by(id=user_id).first_or_404()
    user.status=True
    db.session.commit()
    flash("user activated!", 'success')
    return redirect(url_for('main.users'))

@main.route('/deactivate/<user_id>')
@login_required
def deactivate(user_id):
    if current_user.role.permissions.write_user != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    user = User.query.filter_by(id=user_id).first_or_404()
    if user.id == 1 or user==current_user:
        flash("can't deactivate superadmin or yourself", 'danger')
        return redirect(url_for('main.users'))
    user.status=False
    db.session.commit()
    flash("user deactivated!", 'success')
    return redirect(url_for('main.users'))


@main.route('/role_permissions', methods=['GET', 'POST'])
@login_required
def role_permissions():
    if current_user.role.permissions.read_user != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    roles = Role.query.order_by(Role.created_at.desc()).all()
    return render_template('admin/role_permissions.html', roles=roles, title='Roles & Permission')


@main.route('/role_permissions_values', methods=['GET', 'POST'])
@login_required
def role_permissions_values():
    role = Role.query.order_by(Role.created_at.desc()).first()
    if role is None:
        return jsonify({'status': 0})
    if request.method == 'POST':
        role = Role.query.filter_by(id=request.form['id']).first_or_404()
    response = jsonify({'status': 1, 'data': role.to_dict_role()})
    return response


@main.route('/edit_role_permissions_js', methods=['GET', 'POST'])
@login_required
def edit_role_permissions_js():
    role = Role.query.filter_by(id=request.form['id']).first_or_404()
    data = request.form
    role.from_dict(data)
    role.permissions.from_dict_permissions(data)
    db.session.commit()
    return jsonify({'status': 1})


@main.route('/company_settings', methods=['GET', 'POST'])
@login_required
def company_settings():
    if current_user.role.permissions.read_user != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    company = Company.query.first()
    if company is None:
        form = CompanyForm()
        if form.validate_on_submit():
            company = Company(company_name=form.company_name.data, contact_person=form.contact_person.data,
                              phone_number=form.phone_number.data, email=form.email.data, address=form.address.data,
                              country=form.country.data, city=form.city.data, state=form.state.data,
                              postal_code=form.postal_code.data, bank_name=form.bank_name.data,
                              bank_country=form.bank_country.data, bank_address=form.bank_address.data,
                              account_number=form.account_number.data)
            db.session.add(company)
            db.session.commit()
            flash('company settings updated successfully', 'success')
            return redirect(url_for('main.company_settings'))
    else:
        form = CompanyForm(obj=company)
        if form.validate_on_submit():
            form.populate_obj(company)
            db.session.commit()
            flash('company settings updated successfully', 'success')
            return redirect(url_for('main.company_settings'))
    return render_template('admin/company_settings.html', form=form, title='Company Settings')


@main.route('/theme_settings', methods=['GET', 'POST'])
@login_required
def theme_settings():
    if current_user.role.permissions.read_user != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    theme = Theme.query.first()
    if theme is None:
        form = ThemeForm()
        if form.validate_on_submit():
            photos = UploadSet('photos', IMAGES)
            logo = photos.save(form.logo.data)
            favicon = photos.save(form.favicon.data)
            theme = Theme(company_name=form.company_name.data, logo=logo, favicon=favicon)
            db.session.add(theme)
            db.session.commit()
            flash('theme settings updated successfully', 'success')
            return redirect(url_for('main.theme_settings'))
    else:
        form = ThemeForm(obj=theme)
        if form.validate_on_submit():
            photos = UploadSet('photos', IMAGES)
            logo = photos.save(form.logo.data)
            favicon = photos.save(form.favicon.data)
            theme.company_name = form.company_name.data
            theme.logo = logo
            theme.favicon = favicon
            db.session.commit()
            flash('theme settings updated successfully', 'success')
            return redirect(url_for('main.theme_settings'))
    return render_template('admin/theme_settings.html', form=form, title='Theme Settings')


@main.route('/settings', methods=['GET', 'POST'])
@login_required
def settings():
    form = EditPasswordForm()
    if form.validate_on_submit():
        current_user.set_password(form.password.data)
        db.session.commit()
        flash('Password changed successfully', 'success')
        return redirect(url_for('main.settings'))
    return render_template('admin/settings.html', form=form, title='Edit Password')
#end of admin

@main.route('/inventory', methods=['GET', 'POST'])
@login_required
def inventory():
    if current_user.role.permissions.read_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    g.search_form.table.data = 'Inventory'
    page = request.args.get('page', 1, type=int)
    inventory = Inventory.query.order_by(Inventory.created_at.desc()).paginate(page,current_app.config['POSTS_PER_PAGE'],False)
    next_url = url_for('main.inventory', page=inventory.next_num) if inventory.has_next else None
    prev_url = url_for('main.inventory', page=inventory.prev_num) if inventory.has_prev else None
    return render_template('admin/inventory.html', inventory=inventory.items, next_url=next_url, prev_url=prev_url, title='inventory')

@main.route('/sold_cars', methods=['GET', 'POST'])
@login_required
def sold_cars():
    if current_user.role.permissions.read_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    g.search_form.table.data = 'Inventory'
    page = request.args.get('page', 1, type=int)
    inventory = Inventory.query.filter_by(state='sold').order_by(Inventory.created_at.desc()).paginate(page,current_app.config['POSTS_PER_PAGE'],False)
    next_url = url_for('main.inventory', page=inventory.next_num) if inventory.has_next else None
    prev_url = url_for('main.inventory', page=inventory.prev_num) if inventory.has_prev else None
    return render_template('admin/inventory.html', inventory=inventory.items, next_url=next_url, prev_url=prev_url, title='inventory')

@main.route('/reserved_cars', methods=['GET', 'POST'])
@login_required
def reserved_cars():
    if current_user.role.permissions.read_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    g.search_form.table.data = 'Inventory'
    page = request.args.get('page', 1, type=int)
    inventory = Inventory.query.filter_by(state='reserved').order_by(Inventory.created_at.desc()).paginate(page,current_app.config['POSTS_PER_PAGE'],False)
    next_url = url_for('main.inventory', page=inventory.next_num) if inventory.has_next else None
    prev_url = url_for('main.inventory', page=inventory.prev_num) if inventory.has_prev else None
    return render_template('admin/inventory.html', inventory=inventory.items, next_url=next_url, prev_url=prev_url, title='inventory')

@main.route('/enquiry', methods=['GET', 'POST'])
@login_required
def enquiry():
    if current_user.role.permissions.read_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    enquiry = Enquiry.query.order_by(Enquiry.created_at.desc()).all()
    return render_template('admin/enquiries.html', enquiries=enquiry, title='Enquiry')

@main.route('/bazaar', methods=['GET', 'POST'])
@login_required
def bazaar():
    if current_user.role.permissions.read_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    bazaar = Bazaar.query.order_by(Bazaar.created_at.desc()).all()
    return render_template('admin/bazaar.html', bazaars=bazaar, title='Bazaar')

@main.route('/make', methods=['GET', 'POST'])
@login_required
def make():
    if current_user.role.permissions.read_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    make = Vehicle.query.all()
    return render_template('admin/make.html', makes=make, title='Makes')

# @main.route('/_get_vehicle', methods=['GET', 'POST'])
# @login_required
# def _get_vehicle():
#
#     page = request.args.get('start', 1, type=int)
#     per_page = min(request.args.get('length', 10, type=int), 100)
#     search =request.args.get('search[value]')
#     order=request.args.get('order[0][column]')
#     order_dir=request.args.get('order[0][dir]')
#     if page == 0:
#         page=1
#     else:
#         page=int((page/per_page)+1)
#     data = Vehicle.to_collection_table_dict(page, per_page,search,order,order_dir)
#     return jsonify(data)

@main.route('/imports', methods=['GET', 'POST'])
@login_required
def imports():
    if current_user.role.permissions.read_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    imports = Import.query.order_by(Import.created_at.desc()).all()
    return render_template('admin/import.html', imports=imports, title='Imports')

@main.route('/add_inventory', methods=['GET', 'POST'])
@login_required
def add_inventory():
    if current_user.role.permissions.create_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    form = InventoryForm()
    form.year.choices=[(row.year, row.year) for row in Year.query.order_by(Year.year.desc()).all()]
    form.make.choices=[(row.make, row.make) for row in Vehicle.query.group_by(Vehicle.make)]
    form.model.choices=[(row.model, row.model) for row in Vehicle.query.group_by(Vehicle.model)]
    if form.validate_on_submit():
        image = form.image.data
        if image is not None:
            image = photos.save(form.image.data)
        model= Vehicle.query.filter_by(make=form.make.data,model=form.model.data).first_or_404()
        inventory = Inventory(vehicle=model,year=form.year.data,image=image, plate=form.plate.data,color=form.color.data,price=form.price.data, mileage=form.mileage.data,
                              fuel=form.fuel.data, transmission=form.transmission.data, engine=form.engine.data, body=form.body.data,arrival_date=form.arrival_date.data,state='available')
        db.session.add(inventory)
        db.session.commit()
        flash('Inventory added successfully', 'success')
        return redirect(url_for('main.inventory'))
    return render_template('admin/add-inventory.html', form=form, title='add inventory')

@main.route('/create_enquiry', methods=['GET', 'POST'])
@login_required
def create_enquiry():
    if current_user.role.permissions.create_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    form = EnquiryForm()
    form.client_id.choices=[(row.id, row.client_name) for row in Client.query.order_by(Client.created_at.desc()).all()]
    form.year.choices=[(row.year, row.year) for row in Year.query.order_by(Year.year.desc()).all()]
    form.make.choices=[(row.make, row.make) for row in Vehicle.query.group_by(Vehicle.make)]
    form.model.choices=[(row.model, row.model) for row in Vehicle.query.group_by(Vehicle.model)]
    if form.validate_on_submit():
        client=Client.query.filter_by(id=form.client_id.data).first_or_404()
        model= Vehicle.query.filter_by(make=form.make.data,model=form.model.data).first_or_404()
        enquiry = Enquiry(vehicle=model,client=client,year=form.year.data, budget=form.budget.data, author=current_user)
        db.session.add(enquiry)
        db.session.commit()
        flash('Enquiry added successfully', 'success')
        return redirect(url_for('main.enquiry'))
    return render_template('admin/create_enquiry.html', form=form, title='create enquiry')

@main.route('/create_import', methods=['GET', 'POST'])
@login_required
def create_import():
    if current_user.role.permissions.create_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    form = ImportForm()
    form.client_id.choices=[(row.id, row.client_name) for row in Client.query.order_by(Client.created_at.desc()).all()]
    form.year.choices=[(row.year, row.year) for row in Year.query.order_by(Year.year.desc()).all()]
    form.make.choices=[(row.make, row.make) for row in Vehicle.query.group_by(Vehicle.make)]
    form.model.choices=[(row.model, row.model) for row in Vehicle.query.group_by(Vehicle.model)]
    if form.validate_on_submit():
        client=Client.query.filter_by(id=form.client_id.data).first_or_404()
        model= Vehicle.query.filter_by(make=form.make.data,model=form.model.data).first_or_404()
        import_car = Import(vehicle=model,client=client, budget=form.budget.data,year=form.year.data, color=form.color.data, fuel=form.fuel.data, transmission=form.transmission.data, engine=form.engine.data, special_features=form.special_features.data)
        db.session.add(import_car)
        db.session.commit()
        flash('Import added successfully', 'success')
        return redirect(url_for('main.imports'))
    return render_template('admin/create_import.html', form=form, title='Add import')

@main.route('/create_bazaar', methods=['GET', 'POST'])
@login_required
def create_bazaar():
    if current_user.role.permissions.create_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    form = BazaarForm()
    if form.validate_on_submit():
        bazaar = Bazaar(name=form.name.data,location=form.location.data, phone=form.phone.data,color=str(form.color.data))
        db.session.add(bazaar)
        db.session.commit()
        flash('Bazaar added successfully', 'success')
        return redirect(url_for('main.bazaar'))
    return render_template('admin/create_bazaar.html', form=form, title='Create Bazaar')

@main.route('/add_make', methods=['GET', 'POST'])
@login_required
def add_make():
    if current_user.role.permissions.create_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    form = AddMakeForm()
    if form.validate_on_submit():
        vehicle = Vehicle(model=form.model.data.upper(), make=form.make.data.upper())
        db.session.add(vehicle)
        db.session.commit()
        flash('Make added successfully', 'success')
        return redirect(url_for('main.make'))
    return render_template('admin/add_make.html', form=form, title='Add Make')

@main.route('/edit_inventory/<id>', methods=['GET', 'POST'])
@login_required
def edit_inventory(id):
    if current_user.role.permissions.write_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    inventory = Inventory.query.filter_by(id=id).first_or_404()
    form = InventoryForm(obj=inventory)
    form.year.choices=[(row.year, row.year) for row in Year.query.order_by(Year.year.desc()).all()]
    form.make.choices=[(row.make, row.make) for row in Vehicle.query.group_by(Vehicle.make)]
    form.model.choices=[(row.model, row.model) for row in Vehicle.query.group_by(Vehicle.model)]
    if form.validate_on_submit():
        image = form.image.data
        if image is not None and inventory.image != image:
            image = photos.save(form.image.data)
            form.image.data = image
        form.populate_obj(inventory)
        db.session.commit()
        flash('Inventory edited ', 'success')
        return redirect(url_for('main.inventory'))
    return render_template('admin/edit-inventory.html', inventory=inventory, form=form, title='edit inventory')

@main.route('/add_external_price/<id>', methods=['POST'])
@login_required
def add_external_price(id):
    if current_user.role.permissions.create_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    form = ExternalPriceForm()
    if form.validate_on_submit():
        inventory = Inventory.query.filter_by(id=id).first_or_404()
        inventory.external_price=form.external_price.data
        db.session.commit()
        return jsonify({'status': 1, 'message': 'external price added successfully'})
    return jsonify(data=form.errors)

@main.route('/add_bazaar/<id>', methods=['POST'])
@login_required
def add_bazaar(id):
    if current_user.role.permissions.create_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    form = AddBazaarForm()
    form.bazaar.choices=[(row.id, row.name) for row in Bazaar.query.order_by(Bazaar.created_at.desc()).all()]
    if form.validate_on_submit():
        inventory = Inventory.query.filter_by(id=id).first_or_404()
        bazaar = Bazaar.query.filter_by(id=form.bazaar.data).first_or_404()
        inventory.bazaar=bazaar
        db.session.commit()
        return jsonify({'status': 1, 'message': 'bazaar updated successfully'})
    return jsonify(data=form.errors)

@main.route('/edit_enquiry/<id>', methods=['GET', 'POST'])
@login_required
def edit_enquiry(id):
    if current_user.role.permissions.write_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    enquiry = Enquiry.query.filter_by(id=id).first_or_404()
    form = EnquiryForm(obj=enquiry)
    form.client_id.choices=[(row.id, row.client_name) for row in Client.query.order_by(Client.created_at.desc()).all()]
    form.year.choices=[(row.year, row.year) for row in Year.query.order_by(Year.year.desc()).all()]
    form.make.choices=[(row.make, row.make) for row in Vehicle.query.group_by(Vehicle.make)]
    form.model.choices=[(row.model, row.model) for row in Vehicle.query.group_by(Vehicle.model)]
    if form.validate_on_submit():
        form.populate_obj(enquiry)
        db.session.commit()
        flash('Enquiry edited ', 'success')
        return redirect(url_for('main.enquiry'))
    return render_template('admin/edit_enquiry.html', form=form, title='edit enquiry')

@main.route('/edit_bazaar/<id>', methods=['GET', 'POST'])
@login_required
def edit_bazaar(id):
    if current_user.role.permissions.write_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    bazaar = Bazaar.query.filter_by(id=id).first_or_404()
    form = BazaarForm(obj=bazaar)
    if form.validate_on_submit():
        bazaar.color=str(form.color.data)
        bazaar.name=form.name.data
        bazaar.location=form.location.data
        bazaar.phone=form.phone.data
        db.session.commit()
        flash('Bazaar edited ', 'success')
        return redirect(url_for('main.bazaar'))
    return render_template('admin/create_bazaar.html', form=form, title='edit Bazaar')

@main.route('/edit_import/<id>', methods=['GET', 'POST'])
@login_required
def edit_import(id):
    if current_user.role.permissions.write_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    import_car = Import.query.filter_by(id=id).first_or_404()
    form = ImportForm(obj=import_car)
    form.client_id.choices=[(row.id, row.client_name) for row in Client.query.order_by(Client.created_at.desc()).all()]
    form.year.choices=[(row.year, row.year) for row in Year.query.order_by(Year.year.desc()).all()]
    form.make.choices=[(row.make, row.make) for row in Vehicle.query.group_by(Vehicle.make)]
    form.model.choices=[(row.model, row.model) for row in Vehicle.query.group_by(Vehicle.model)]
    if form.validate_on_submit():
        form.populate_obj(import_car)
        db.session.commit()
        flash('Import edited ', 'success')
        return redirect(url_for('main.imports'))
    return render_template('admin/edit_import.html', form=form, title='edit import')

@main.route('/view_inventory/<id>')
@login_required
def view_inventory(id):
    if current_user.role.permissions.read_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    inventory = Inventory.query.filter_by(id=id).first_or_404()
    form = ExternalPriceForm(obj=inventory)
    baz_form = AddBazaarForm()
    baz_form.bazaar.choices=[(row.id, row.name) for row in Bazaar.query.order_by(Bazaar.created_at.desc()).all()]
    return render_template('admin/view-inventory.html', inventory=inventory,form=form,baz_form=baz_form, title='view inventory')

@main.route('/view_bazaar/<id>', methods=['GET', 'POST'])
@login_required
def view_bazaar(id):
    if current_user.role.permissions.read_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    g.search_form.table.data = 'Inventory'
    page = request.args.get('page', 1, type=int)
    inventory = Inventory.query.filter_by(bazaar_id=id).order_by(Inventory.created_at.desc()).paginate(page,current_app.config['POSTS_PER_PAGE'],False)
    next_url = url_for('main.view_bazaar',id=id, page=inventory.next_num) if inventory.has_next else None
    prev_url = url_for('main.view_bazaar',id=id, page=inventory.prev_num) if inventory.has_prev else None
    return render_template('admin/inventory.html', inventory=inventory.items, next_url=next_url, prev_url=prev_url, title='inventory')
##finance starts here
#invoice
@main.route('/invoices')
@login_required
def invoices():
    if current_user.role.access_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    invoices = Invoice.query.order_by(Invoice.created_at.desc()).all()
    return render_template('admin/invoices.html', title = 'Invoice', invoices=invoices)

#quotations
@main.route('/quotations')
@login_required
def quotations():
    if current_user.role.access_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    quotations = Quotation.query.order_by(Quotation.created_at.desc()).all()
    return render_template('admin/quotations.html', title = 'Quotations', quotations=quotations)

#bills
@main.route('/bills')
@login_required
def bills():
    if current_user.role.access_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    bills = Bill.query.order_by(Bill.created_at.desc()).all()
    return render_template('admin/bills.html', title = 'Bill', bills=bills)

#payments
@main.route('/payments')
@login_required
def payments():
    if current_user.role.access_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    payments = Payment.query.order_by(Payment.created_at.desc()).all()
    return render_template('admin/payments.html', title = 'Payment', payments=payments)

#sell_car
@main.route('/sell_requests')
@login_required
def sell_requests():
    if current_user.role.access_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    requests = Sellcar.query.order_by(Sellcar.created_at.desc()).all()
    return render_template('admin/sell_requests.html', title = 'Sell requests', requests=requests)

#trade_in
@main.route('/trade_in')
@login_required
def trade_in():
    if current_user.role.access_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    requests = Tradein.query.order_by(Tradein.created_at.desc()).all()
    return render_template('admin/trade_in.html', title = 'Trade-in', requests=requests)

#import
@main.route('/import_requests')
@login_required
def import_requests():
    if current_user.role.access_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    requests = Importcar.query.order_by(Importcar.created_at.desc()).all()
    return render_template('admin/import_requests.html', title = 'Import requests', requests=requests)

#contact us
@main.route('/contact_us')
@login_required
def contact_us():
    if current_user.role.access_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    requests = Contact.query.order_by(Contact.created_at.desc()).all()
    return render_template('admin/contact_us.html', title = 'Contact us', requests=requests)

#Clients
@main.route('/clients', methods=['GET', 'POST'])
@login_required
def clients():
    if current_user.role.access_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    form = ClientForm()
    if form.validate_on_submit():
        client = Client(client_name=form.client_name.data, client_type=form.client_type.data, phone_number=form.phone_number.data, email=form.email.data, location=form.location.data )
        db.session.add(client)
        db.session.commit()
        flash('Client added successfully', 'success')
        return redirect(url_for('main.clients'))
    clients = Client.query.order_by(Client.created_at.desc()).all()
    return render_template('admin/clients.html',title= 'clients', form=form, clients=clients)

#suppliers
@main.route('/suppliers', methods=['GET', 'POST'])
@login_required
def suppliers():
    if current_user.role.access_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    form = SupplierForm()
    if form.validate_on_submit():
        supplier = Supplier(supplier_name=form.supplier_name.data, supplier_type=form.supplier_type.data, phone_number=form.phone_number.data, email=form.email.data,
                            location=form.location.data, bank_name=form.bank_name.data,bank_country=form.bank_country.data,
                            bank_address=form.bank_address.data, account_number=form.account_number.data)
        db.session.add(supplier)
        db.session.commit()
        flash('Supplier added successfully', 'success')
        return redirect(url_for('main.suppliers'))
    suppliers = Supplier.query.order_by(Supplier.created_at.desc()).all()
    return render_template('admin/suppliers.html',title= 'suppliers', form=form, suppliers=suppliers)

#create quotation
@main.route('/create_quotation', methods=['GET', 'POST'])
@login_required
def create_quotation():
    if current_user.role.permissions.create_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    form = QuotationForm()
    form.client_id.choices = [(row.id, row.client_name) for row in Client.query.order_by(Client.created_at.desc()).all()]
    if form.validate_on_submit():
        quotation_number = 'QUO-001'
        quotation = Quotation.query.order_by(Quotation.created_at.desc()).first()
        if quotation is not None:
            quot_no= quotation.quotation_no.strip('QUO-')
            no=1
            quotation_no =int(quot_no) + int(no)
            quotation_number="QUO-00" + str(quotation_no)
        client = Client.query.filter_by(id=form.client_id.data).first_or_404()
        quotation = Quotation(quotation_name=form.quotation_name.data, quotation_no=quotation_number, quotation_date=form.quotation_date.data,
                              description=form.description.data, amount=form.amount.data,client=client)
        db.session.add(quotation)
        db.session.commit()
        quotation_id = Quotation.query.order_by(Quotation.created_at.desc()).first()
        for item in form.products.data:
            products  = Product(product=item['product'], price = item['price'], quantity=item['quantity'], size=item['size'], total=item['total'], quotation=quotation_id)
            db.session.add(products)
            db.session.commit()
        flash('Quotation added successfully', 'success')
        return redirect(url_for('main.quotations'))

    return render_template('admin/create_quotation.html', title="create Quotation", form=form)

#create invoice
@main.route('/create_invoice', methods=['POST'])
@login_required
def create_invoice():
    if current_user.role.permissions.create_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    invoice_number = 'INV-001'
    invoice = Invoice.query.order_by(Invoice.created_at.desc()).first()
    if invoice is not None:
        inv_no= invoice.invoice_no.strip('INV-')
        no=1
        invoice_no =int(inv_no) + int(no)
        invoice_number="INV-00" + str(invoice_no)
    id = request.form['id']
    date_due = request.form['due_date']
    date = datetime.strptime(date_due, '%Y-%m-%d')
    due_date = datetime.combine(date, datetime.min.time())
    quotation = Quotation.query.filter_by(id=id).first_or_404()
    save_invoice = Invoice(invoice_no=invoice_number, date_due=due_date, quotation =quotation)
    db.session.add(save_invoice)
    db.session.commit()
    invoice = Invoice.query.order_by(Invoice.created_at.desc()).first_or_404()
    return jsonify({'status': 1, 'id':invoice.id})

#create bill
@main.route('/create_bill', methods=['GET', 'POST'])
@login_required
def create_bill():
    if current_user.role.permissions.create_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    form = BillForm()
    form.supplier_id.choices = [(row.id, row.supplier_name) for row in Supplier.query.order_by(Supplier.created_at.desc()).all()]

    if form.validate_on_submit():

        bill_number = 'BIL-001'
        bill = Bill.query.order_by(Bill.created_at.desc()).first()
        if bill is not None:
            bi_no= bill.bill_no.strip('BIL-')
            no=1
            bill_no =int(bi_no) + int(no)
            bill_number="BIL-00" + str(bill_no)
        supplier = Supplier.query.filter_by(id=form.supplier_id.data).first_or_404()
        bill = Bill(bill_name=form.bill_name.data, bill_no=bill_number, date_due=form.date_due.data,
                              description=form.description.data, amount=form.amount.data,supplier=supplier)
        db.session.add(bill)
        db.session.commit()
        flash('Bill added successfully', 'success')
        return redirect(url_for('main.bills'))
    return render_template('admin/create_bill.html', title="create Bill", form=form)

#create payment
@main.route('/create_payment', methods=['GET', 'POST'])
@login_required
def create_payment():
    if current_user.role.permissions.create_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    form = PaymentForm()
    form.invoice_id.choices = [(row.id, row.quotation.quotation_name) for row in Invoice.query.order_by(Invoice.created_at.desc()).all()]

    if form.validate_on_submit():
        invoice = Invoice.query.filter_by(id=form.invoice_id.data).first_or_404()
        payment = Payment(payment_mode=form.payment_mode.data, payment_type=form.payment_type.data,
                              note=form.note.data, amount=form.amount.data,invoice=invoice)
        db.session.add(payment)
        db.session.commit()
        flash('Payment added successfully', 'success')
        return redirect(url_for('main.payments'))
    return render_template('admin/create_payment.html', title="create Payment", form=form)

#edit quotation
@main.route('/edit_quotation/<id>', methods=['GET', 'POST'])
@login_required
def edit_quotation(id):
    if current_user.role.permissions.write_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    quotation = Quotation.query.filter_by(id=id).first_or_404()
    form=EditQuotationForm(obj=quotation)
    form.client_id.choices = [(row.id, row.client_name) for row in Client.query.order_by(Client.created_at.desc()).all()]
    if form.validate_on_submit():
        form.populate_obj(quotation)
        db.session.commit()
        flash('Quotation edited successfully', 'success')
        return redirect(url_for('main.quotations'))
    return render_template('admin/edit_quotation.html',form=form)

#edit bill
@main.route('/edit_bill/<id>', methods=['GET', 'POST'])
@login_required
def edit_bill(id):
    if current_user.role.permissions.write_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    bill = Bill.query.filter_by(id=id).first_or_404()
    form = BillForm(obj = bill)
    form.supplier_id.choices =[(row.id, row.supplier_name) for row in Supplier.query.order_by(Supplier.created_at.desc()).all()]

    if form.validate_on_submit():
        form.populate_obj(bill)
        db.session.commit()
        flash('Bill edited successfully', 'success')
        return redirect(url_for('main.bills'))

    return render_template('admin/edit_bill.html',form=form, title='Edit Bill')

#edit payment
@main.route('/edit_payment/<id>', methods=['GET', 'POST'])
@login_required
def edit_payment(id):
    if current_user.role.permissions.write_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    payment = Payment.query.filter_by(id=id).first_or_404()
    form = PaymentForm(obj = payment)
    form.invoice_id.choices = [(row.id, row.quotation.quotation_name) for row in Invoice.query.order_by(Invoice.created_at.desc()).all()]
    if form.validate_on_submit():
        form.populate_obj(payment)
        db.session.commit()
        flash('Payment edited successfully', 'success')
        return redirect(url_for('main.payments'))
    return render_template('admin/edit_payment.html',form=form, title='Edit Payment')

#edit client
@main.route('/edit_client/<id>', methods=['GET', 'POST'])
@login_required
def edit_client(id):
    client = Client.query.filter_by(id=id).first_or_404()
    form = ClientForm(obj=client)
    if form.validate_on_submit():
        form.populate_obj(client)
        db.session.commit()
        flash('Client edited successfully', 'success')
        return redirect(url_for('main.clients'))
    elif request.method == 'GET':
        data={'client_type':client.client_type, 'client_name':client.client_name,'phone_number':client.phone_number,'email':client.email,'location':client.location }
        return jsonify({'status': 1, 'data':data})

#edit supplier
@main.route('/edit_supplier/<id>', methods=['GET', 'POST'])
@login_required
def edit_supplier(id):
    supplier = Supplier.query.filter_by(id=id).first_or_404()
    form = SupplierForm(obj=supplier)
    if form.validate_on_submit():
        form.populate_obj(supplier)
        db.session.commit()
        flash('Supplier edited successfully', 'success')
        return redirect(url_for('main.suppliers'))
    elif request.method == 'GET':
        data={'supplier_type':supplier.supplier_type, 'supplier_name':supplier.supplier_name,'phone_number':supplier.phone_number,'email':supplier.email,
              'location':supplier.location,'bank_name':supplier.bank_name,'bank_country':supplier.bank_country,'bank_address':supplier.bank_address,'account_number':supplier.account_number }
        return jsonify({'status': 1, 'data':data})

#view invoice
@main.route('/view_invoice/<id>')
@login_required
def view_invoice(id):
    if current_user.role.permissions.read_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    invoice = Invoice.query.filter_by(id=id).first_or_404()
    return render_template('admin/view_invoice.html', title = 'view invoice', invoice=invoice)

#view quotation
@main.route('/view_quotation/<id>')
@login_required
def view_quotation(id):
    if current_user.role.permissions.read_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    quotation = Quotation.query.filter_by(id=id).first_or_404()
    return render_template('admin/view_quotation.html', title = 'view quotation', quotation=quotation)

#view bill
@main.route('/view_bill/<id>')
@login_required
def view_bill(id):
    if current_user.role.permissions.read_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    bill = Bill.query.filter_by(id=id).first_or_404()
    return render_template('admin/view_bill.html', title = 'view bill', bill=bill)

#view payment
@main.route('/view_payment/<id>')
@login_required
def view_payment(id):
    if current_user.role.permissions.read_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    payment = Payment.query.filter_by(id=id).first_or_404()
    return render_template('admin/view_payment.html', title = 'view payment', payment=payment)

#view clients
@main.route('/view_client/<id>')
@login_required
def view_client(id):
    if current_user.role.permissions.read_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    client = Client.query.filter_by(id=id).first_or_404()
    return render_template('admin/view_client.html', title = 'view client', client=client)

#view suppliers
@main.route('/view_supplier/<id>')
@login_required
def view_supplier(id):
    if current_user.role.permissions.read_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    supplier = Supplier.query.filter_by(id=id).first_or_404()
    return render_template('admin/view_supplier.html', title = 'view supplier', supplier=supplier)
##endfinance

@main.route('/reserve_car/<id>')
@login_required
def reserve_car(id):
    if current_user.role.permissions.write_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    inventory = Inventory.query.filter_by(id=id).first_or_404()
    inventory.state='reserved'
    inventory.reserver=current_user
    db.session.commit()
    flash("Car reserved successfully!", 'success')
    return redirect(url_for('main.view_inventory',id=id))
#return reserved
@main.route('/return_reserved/<id>')
@login_required
def return_reserved(id):
    if current_user.role.permissions.write_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    inventory = Inventory.query.filter_by(id=id).first_or_404()
    inventory.state='available'
    db.session.commit()
    flash("Car returned successfully!", 'success')
    return redirect(url_for('main.view_inventory',id=id))

#set featured

@main.route('/set_featured/<id>', methods=['GET', 'POST'])
@login_required
def set_featured(id):
    if current_user.role.permissions.write_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    if request.method == 'POST':
        inventory = Inventory.query.filter_by(id=id).first_or_404()
        if request.form['featured'] == 'false':
            inventory.featured=False
        elif request.form['featured'] == 'true':
            inventory.featured=True
        db.session.commit()
        return jsonify({'status': 1, 'message': 'Car set as featured'})
    return jsonify(status=0)

@main.route('/sell_car/<id>')
@login_required
def sell_car(id):
    if current_user.role.permissions.write_inventory != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    inventory = Inventory.query.filter_by(id=id).first_or_404()
    if inventory.reserver != current_user:
        flash("This car was reserved by another user!", 'danger')
        return redirect(url_for('main.view_inventory',id=id))
    inventory.state='sold'
    db.session.commit()
    flash("Car marked as sold!", 'success')
    return redirect(url_for('main.view_inventory',id=id))

@main.route('/_get_model/')
def _get_model():
    make = request.args.get('make', '01', type=str)
    model =[(row.model, row.model) for row in Vehicle.query.filter_by(make=make).group_by(Vehicle.model)]
    return jsonify(model)

@main.route('/_get_year/')
def _get_year():
    make = request.args.get('make', '01', type=str)
    model = request.args.get('model', '01', type=str)
    list=[(row.id) for row in Vehicle.query.filter_by(make=make,model=model).all()]
    year=None
    if list:
        year=[(row.year, row.year) for row in Inventory.query.filter(Inventory.vehicle_id.in_(list)).order_by(Inventory.year.desc()).group_by(Inventory.year)]
    return jsonify(year)

@main.route('/delete', methods=['GET', 'POST'])
@login_required
def delete():
    id = request.form['id']
    tablename = request.form['table']
    if tablename == 'Inventory':
        table = Inventory.query.filter_by(id=id).first_or_404()
    elif tablename == 'Client':
        table = Client.query.filter_by(id=id).first_or_404()
    elif tablename == 'Quotation':
        table = Quotation.query.filter_by(id=id).first_or_404()
    elif tablename == 'Invoice':
        table = Invoice.query.filter_by(id=id).first_or_404()
    elif tablename == 'Payment':
        table = Payment.query.filter_by(id=id).first_or_404()
    elif tablename == 'Supplier':
        table = Supplier.query.filter_by(id=id).first_or_404()
    elif tablename == 'Bill':
        table = Bill.query.filter_by(id=id).first_or_404()
    elif tablename == 'Enquiry':
        table = Enquiry.query.filter_by(id=id).first_or_404()
    elif tablename == 'Importcar':
        table = Importcar.query.filter_by(id=id).first_or_404()
    elif tablename == 'Sellcar':
        table = Sellcar.query.filter_by(id=id).first_or_404()
    elif tablename == 'Tradein':
        table = Tradein.query.filter_by(id=id).first_or_404()
    elif tablename == 'Contact':
        table = Contact.query.filter_by(id=id).first_or_404()
    elif tablename == 'Import':
        table = Import.query.filter_by(id=id).first_or_404()
    elif tablename == 'Vehicle':
        table = Vehicle.query.filter_by(id=id).first_or_404()
    elif tablename == 'User':
        table = User.query.filter_by(id=id).first_or_404()
        if table.status == True:
            return jsonify({'status': 0,'message':'User must be deactivated first'})
        if table.id == 1:
            return jsonify({'status': 0,'message':'Cannot delete superadmin'})
    elif tablename == 'Role':
            table = Role.query.filter_by(id=id).first_or_404()
    elif tablename == 'Bazaar':
            table = Bazaar.query.filter_by(id=id).first_or_404()
    db.session.delete(table)
    db.session.commit()
    return jsonify({'status': 1})

@main.route('/search')
@login_required
def search():
    if not g.search_form.validate():
        return redirect(url_for('main.index'))
    search = g.search_form.q.data
    tablename = g.search_form.table.data
    if tablename == 'Inventory':
        list=[(row.id) for row in Vehicle.query.filter(or_(Vehicle.make.like('%' + search.upper() + '%'),Vehicle.model.like('%' + search.upper() + '%'))).all()]
        table=None
        if list:
            table=Inventory.query.filter(Inventory.vehicle_id.in_(list)).order_by(Inventory.created_at.desc()).all()
        return render_template('admin/inventory.html', inventory=table, title='Inventory')
