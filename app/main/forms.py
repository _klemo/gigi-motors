from flask_wtf import FlaskForm
from wtforms import Form as NoCsrfForm
from wtforms.fields import BooleanField, StringField, SubmitField, FloatField, PasswordField, IntegerField, DateField, SelectField, FieldList, TextAreaField, FormField, HiddenField, DecimalField, FloatField
from wtforms.validators import DataRequired, Email, EqualTo
from wtforms import ValidationError
from app.models import *
from flask_login import current_user
from flask_uploads import UploadSet, configure_uploads, IMAGES
from flask_wtf.file import FileField, FileRequired, FileAllowed
from wtforms_components import ColorField
from flask_babel import _, lazy_gettext as _l
from flask import request, g
from sqlalchemy import and_
photos = UploadSet('photos', IMAGES)


class InventoryForm(FlaskForm):
    year = SelectField(validators=[DataRequired()],coerce=int,id="car-years")
    make = SelectField(validators=[DataRequired()],id="car-makes")
    model =SelectField(validators=[DataRequired()],id="car-models")
    image = FileField(validators=[FileAllowed(photos, u'Image only!')])
    plate = StringField(validators=[DataRequired()])
    color =SelectField(validators=[DataRequired()],choices=[('black', 'Black'), ('beige', 'Beige'), ('white', 'White'), ('blue', 'Blue'), ('red', 'Red'), ('brown', 'Brown'), ('silver', 'Silver'), ('pearl', 'Pearl'),('orange', 'Orange'),
                                                            ('purple', 'Purple'), ('green', 'Green'), ('red-wine', 'Red-wine'), ('grey', 'Grey'), ('gold', 'Gold'), ('pink', 'Pink')])
    body =SelectField(validators=[DataRequired()],choices=[('Compact', 'Compact'), ('Convertible', 'Convertible'), ('Coupe', 'Coupe'), ('Hatchback', 'Hatchback'), ('Mid-size-suv', 'Mid-Size SUV'), ('Mini-suv', 'Mini SUV'),
                                                         ('Minivan', 'Minivan'),('Off-road', 'Off-Road'),('Pickup', 'Pickup'), ('Saloon', 'Saloon'), ('Sedan', 'Sedan'), ('Sport-car', 'Sport Car'), ('Station-wagons', 'Station Wagons'),
                                                        ('Suv', 'SUV'), ('Vans', 'Vans'), ('Wagon', 'Wagon')])
    fuel =SelectField(validators=[DataRequired()],choices=[('petrol', 'Petrol'), ('diesel', 'diesel')])
    transmission =SelectField(validators=[DataRequired()],choices=[('automatic', 'Automatic'), ('manual', 'Manual')])
    engine            = StringField(validators=[DataRequired()])
    mileage = StringField(validators=[DataRequired()])
    arrival_date = DateField(format='%d/%m/%Y',validators=[DataRequired()])
    price = DecimalField(validators=[DataRequired()])
    submit = SubmitField('Save')

class ExternalPriceForm(FlaskForm):
    external_price = DecimalField(validators=[DataRequired()])
    submit = SubmitField('Save')

class AddBazaarForm(FlaskForm):
    bazaar =SelectField(validators=[DataRequired()],coerce=int)

class ClientForm(FlaskForm):
    client_name  = StringField('client name', validators=[DataRequired()])
    client_type  = StringField('client type', validators=[DataRequired()])
    phone_number = IntegerField('phone number', validators=[DataRequired()])
    email        = StringField('email', validators=[DataRequired(), Email()])
    location     = StringField('location', validators=[DataRequired()])
    submit       = SubmitField('save')


class ProductForm(NoCsrfForm):
    product           = StringField('product', validators=[DataRequired()])
    price             = IntegerField('price', validators=[DataRequired()])
    quantity          = IntegerField('quantity', validators=[DataRequired()])
    size              = IntegerField('size', validators=[DataRequired()])
    total             = IntegerField('total', validators=[DataRequired()])

class QuotationForm(FlaskForm):
    client_id         = SelectField('client_id',coerce=int, id='select_client')
    quotation_name    = StringField('quotation_name', validators=[DataRequired()])
    quotation_date    = DateField('quotation_date',format='%d/%m/%Y')
    products          = FieldList(FormField(ProductForm), min_entries=1)
    amount            = IntegerField('amount', validators=[DataRequired()])
    description       = TextAreaField('description', validators=[DataRequired()])
    submit            = SubmitField('save')

class EditQuotationForm(FlaskForm):
    client_id         = SelectField('client_id',coerce=int, id='select_client')
    quotation_name    = StringField('quotation_name', validators=[DataRequired()])
    quotation_date    = DateField('quotation_date',format='%d/%m/%Y')
    products          = FieldList(FormField(ProductForm,default=lambda: Product()), min_entries=1)
    amount            = IntegerField('amount', validators=[DataRequired()])
    description       = TextAreaField('description', validators=[DataRequired()])
    submit            = SubmitField('save')

class AddMakeForm(FlaskForm):
    make = StringField(validators=[DataRequired()])
    model = StringField(validators=[DataRequired()])
    submit = SubmitField('Save')

class PaymentForm(FlaskForm):
    invoice_id        = SelectField('invoice_id', validators=[DataRequired()],coerce=int, id='select_invoice')
    payment_mode      = SelectField('payment_mode', validators=[DataRequired()],choices=[('cash', 'cash'), ('cheque', 'cheque'), ('online-payment', 'online payment')])
    payment_type      = StringField('payment_type', validators=[DataRequired()])
    amount            = IntegerField('amount', validators=[DataRequired()])
    note              = TextAreaField('note', validators=[DataRequired()])
    submit            = SubmitField('save')

class SupplierForm(FlaskForm):
    supplier_name  = StringField('supplier name name', validators=[DataRequired()])
    supplier_type  = StringField('supplier type', validators=[DataRequired()])
    phone_number   = IntegerField('phone number', validators=[DataRequired()])
    email          = StringField('email', validators=[DataRequired(), Email()])
    location       = StringField('location', validators=[DataRequired()])
    bank_name       = StringField('bank_name', validators=[DataRequired()])
    bank_country    = StringField('bank_country', validators=[DataRequired()])
    bank_address    = StringField('bank_address', validators=[DataRequired()])
    account_number  = IntegerField('account_number', validators=[DataRequired()])
    submit         = SubmitField('save')

class BillForm(FlaskForm):
    supplier_id       = SelectField('supplier_id',coerce=int, id='select_supplier')
    bill_name         = StringField('bill_name', validators=[DataRequired()])
    date_due          = DateField('date_due',format='%d/%m/%Y')
    amount            = IntegerField('amount', validators=[DataRequired()])
    description       = TextAreaField('description', validators=[DataRequired()])
    submit            = SubmitField('save')

class EnquiryForm(FlaskForm):
    client_id         = SelectField('client_id',coerce=int, id='select_client')
    year = SelectField(validators=[DataRequired()],coerce=int,id="car-years")
    make = SelectField(validators=[DataRequired()],id="car-makes")
    model =SelectField(validators=[DataRequired()],id="car-models")
    budget            = DecimalField(validators=[DataRequired()])
    submit            = SubmitField('save')

class BazaarForm(FlaskForm):
    name         = StringField('name', validators=[DataRequired()])
    location = StringField('location', validators=[DataRequired()])
    phone = StringField('location', validators=[DataRequired()])
    color =ColorField(validators=[DataRequired()])
    submit            = SubmitField('Save')

class ImportForm(FlaskForm):
    client_id         = SelectField('client_id',coerce=int, id='select_client')
    year = SelectField(validators=[DataRequired()],coerce=int,id="car-years")
    make = SelectField(validators=[DataRequired()],id="car-makes")
    model =SelectField(validators=[DataRequired()],id="car-models")
    color =SelectField(validators=[DataRequired()],choices=[('black', 'Black'), ('white', 'White'), ('blue', 'Blue'), ('red', 'Red'), ('brown', 'Brown'), ('silver', 'Silver'), ('pearl', 'Pearl'),('orange', 'Orange'),
                                                            ('purple', 'Purple'), ('green', 'Green'), ('red-wine', 'Red-wine'), ('grey', 'Grey'), ('gold', 'Gold'), ('pink', 'Pink')])
    fuel =SelectField(validators=[DataRequired()],choices=[('petrol', 'Petrol'), ('diesel', 'diesel')])
    transmission =SelectField(validators=[DataRequired()],choices=[('automatic', 'Automatic'), ('manual', 'Manual')])
    budget            = DecimalField(validators=[DataRequired()])
    engine            = StringField(validators=[DataRequired()])
    special_features = TextAreaField(validators=[DataRequired()])
    submit            = SubmitField('save')


class EditPasswordForm(FlaskForm):
    oldpassword = PasswordField('Password', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField('Repeat Password', validators=[DataRequired(),
     EqualTo('password')])
    submit = SubmitField('Save')

    def validate_oldpassword(self, oldpassword):
        user = User.query.filter_by(id=current_user.id).first()
        if user is None or not user.check_password(oldpassword.data):
            raise ValidationError('Please enter correct Password.')
        return


class RegistrationForm(FlaskForm):
    username = StringField(_l('Username'), validators=[DataRequired()])
    email = StringField(_l('Email'), validators=[DataRequired(), Email()])
    phone_number = StringField(_l('phone_number'), validators=[DataRequired()])
    role = SelectField('role', coerce=int)
    password = PasswordField(_l('Password'), validators=[DataRequired()])
    password2 = PasswordField(_l('Repeat Password'), validators=[DataRequired(),
     EqualTo('password')])
    submit = SubmitField(_l('Save'))

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError(_('Please use a different email address.'))
        return


class EditRegistrationForm(FlaskForm):
    username = StringField(_l('Username'), validators=[DataRequired()])
    phone_number = StringField(_l('phone_number'), validators=[DataRequired()])
    role = SelectField('role', coerce=int)
    submit = SubmitField(_l('Save'))


class CompanyForm(FlaskForm):
    company_name = StringField('company_name', validators=[DataRequired()])
    contact_person = StringField('contact_person', validators=[DataRequired()])
    address = StringField('address', validators=[DataRequired()])
    country = StringField('country', validators=[DataRequired()])
    city = StringField('city', validators=[DataRequired()])
    state = StringField('state', validators=[DataRequired()])
    postal_code = StringField('postal_code', validators=[DataRequired()])
    email = StringField('email', validators=[DataRequired(), Email()])
    phone_number = IntegerField('phone_number', validators=[DataRequired()])
    bank_name = StringField('bank_name', validators=[DataRequired()])
    bank_country = StringField('bank_country', validators=[DataRequired()])
    bank_address = StringField('bank_address', validators=[DataRequired()])
    account_number = StringField('account_number', validators=[DataRequired()])
    submit = SubmitField('Save & update')


class ThemeForm(FlaskForm):
    company_name = StringField('company_name', validators=[DataRequired()])
    logo = FileField(validators=[FileAllowed(photos, u'Image only!'), FileRequired(u'File was empty!')])
    favicon = FileField(validators=[FileAllowed(photos, u'Image only!'), FileRequired(u'File was empty!')])
    submit = SubmitField(u'Save')


class SearchForm(FlaskForm):
    q = StringField(_l('Search'), validators=[DataRequired()])
    table = HiddenField(_l('table'), validators=[DataRequired()])

    def __init__(self, *args, **kwargs):
        if 'formdata' not in kwargs:
            kwargs['formdata'] = request.args
        if 'csrf_enabled' not in kwargs:
            kwargs['csrf_enabled'] = False
        super(SearchForm, self).__init__(*args, **kwargs)
