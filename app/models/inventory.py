from datetime import datetime
from app import db
from numpy import genfromtxt
from collections import OrderedDict
from flask import current_app, url_for

class PaginatedAPIMixin(object):
    @staticmethod
    def to_collection_dict(query,model, page, per_page, endpoint, **kwargs):
        resources = query.order_by(model.created_at.desc()).paginate(page, per_page, False)
        data = {
            'items': [item.to_dict() for item in resources.items],
            '_meta': {
                'page': page,
                'per_page': per_page,
                'total_pages': resources.pages,
                'total_items': resources.total
            },
            '_links': {
                'self': url_for(endpoint, page=page, per_page=per_page,
                                **kwargs)
            }
        }
        return data

    @staticmethod
    def search_inventory_filters(model,page,search, per_page, endpoint, **kwargs):
        resources = model.query.join(
           Vehicle, (Vehicle.id == model.vehicle_id)).filter(
               (Vehicle.make.like('%' + search.upper() + '%')) |
                   (Vehicle.model.like('%' + search.upper() + '%')) |
                       (model.year.like('%' + search + '%')) |
                          ( model.fuel.like('%' + search + '%')) |
                               (model.transmission.like('%' + search.upper() + '%'))).filter(
                                   model.state=='available').order_by(model.created_at.desc()).paginate(page, per_page, False)

        data = {
            'items': [item.to_dict() for item in resources.items],
            '_meta': {
                'page': page,
                'per_page': per_page,
                'total_pages': resources.pages,
                'total_items': resources.total
            },
            '_links': {
                'self': url_for(endpoint, page=page, per_page=per_page,
                                **kwargs)
            }
        }
        return data

class PaginatedAPIEnquiriesMixin(object):
    @staticmethod
    def to_collection_dict(query,model, page, per_page, endpoint, **kwargs):
        resources = query.order_by(model.created_at.desc()).paginate(page, per_page, False)
        data = {
            'items': [item.to_enquiry_dict() for item in resources.items],
            '_meta': {
                'page': page,
                'per_page': per_page,
                'total_pages': resources.pages,
                'total_items': resources.total
            },
            '_links': {
                'self': url_for(endpoint, page=page, per_page=per_page,
                                **kwargs)
            }
        }
        return data

# class PaginatedAPITableMixin(object):
#     @staticmethod
#     def to_collection_table_dict(page, per_page,search,order,order_dir):
#
#         resources = Vehicle.query.order_by(Vehicle.make.asc()).paginate(page, per_page, False)
#         data = {
#             'data': [item.to_table_dict() for item in resources.items],
#             'recordsTotal': resources.total,
#             'recordsFiltered': resources.total
#             }
#         return data

#inventory
class Inventory(PaginatedAPIMixin,db.Model):
    STATUS= OrderedDict([('available', 'Available'),('reserved', 'Reserved'),('sold', 'Sold')])
    id = db.Column(db.Integer, primary_key=True)
    vehicle_id = db.Column(db.Integer, db.ForeignKey('vehicle.id'))
    plate = db.Column(db.String(64))
    color = db.Column(db.String(64))
    body = db.Column(db.String(64))
    transmission = db.Column(db.String(64))
    fuel = db.Column(db.String(64))
    engine= db.Column(db.String(64))
    year = db.Column(db.Integer)
    mileage = db.Column(db.String(64))
    price = db.Column(db.Numeric)
    external_price = db.Column(db.Numeric)
    featured=db.Column(db.Boolean, default=False)
    reserved_by=db.Column(db.Integer, db.ForeignKey('user.id'), nullable=True)
    bazaar_id=db.Column(db.Integer, db.ForeignKey('bazaar.id'), nullable=True)
    arrival_date = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    state = db.Column(db.Enum(*STATUS, name='state_types', native_enum=False), index=True, nullable=True, server_default='available')
    image = db.Column(db.String(64))
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, index=True, default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<Inventory {}>'.format(self.id)

    def to_dict_body(data):
        list=[]
        for field in ['compact','convertible','coupe','hatchback','mid-size-suv', 'mini-suv','minivan','off-road','pickup','saloon', 'sedan', 'sport-car', 'station-wagons','suv','vans','wagon']:
            if field in data:
                list.append(field.capitalize())
        return list

    def to_dict(self):
        data = {
            'id': self.id,
            'make': self.vehicle.make,
            'model': self.vehicle.model,
            'year': self.year,
            'plate': self.plate,
            'color': self.color,
            'body': self.body,
            'transmission': self.transmission,
            'fuel': self.fuel,
            'engine': self.engine,
            'bazaar_color':self.bazaar.color if self.bazaar else None,
            'mileage': self.mileage,
            'price': self.price,
            'featured': self.featured,
            'reserved_by': self.reserver.username if self.reserved_by else None,
            'state': self.state,
            'arrival_date': self.arrival_date.isoformat() + 'Z',
            '_links': {
                'self': url_for('api.get_inventory', id=self.id),
                'image':url_for('static', filename='uploads/{}'.format(self.image)) if self.image else url_for('static',filename='img/placeholder.png')
            }
        }
        return data

    def from_dict(self, data):
        for field in ['model', 'plate', 'color','body','transmission','fuel','engine','mileage','price','state','arrival_date','image','state','year']:
            if field in data:
                if field =='model':
                    id=data[field]
                    field='vehicle'
                    vehicle=Vehicle.query.get_or_404(id)
                    setattr(self, field, vehicle)
                else:
                    setattr(self, field, data[field])

    def get_inv_make(make,page):
        inventories = Inventory.query.join(
            Vehicle, (Vehicle.id == Inventory.vehicle_id)).filter(
                Vehicle.make.like('%' + make.upper() + '%')).filter(Inventory.state=='available')
        return inventories


    def get_inv_model(make,model,page):
        inventories = Inventory.query.join(
            Vehicle, (Vehicle.id == Inventory.vehicle_id)).filter(
                Vehicle.make.like('%' + make.upper() + '%')).filter(
                    Vehicle.model.like('%' + model.upper() + '%')).filter(Inventory.state=='available')
        return inventories

    def get_inventory_filters(make,model,year,fuel,trans,page):
        inventory = Inventory.query.join(
           Vehicle, (Vehicle.id == Inventory.vehicle_id)).filter(
               Vehicle.make == make if make != 'make' else Vehicle.make.isnot(None)).filter(
                   Vehicle.model== model if model != 'model' else Vehicle.model.isnot(None)).filter(
                       Inventory.year== year if year != 0 else Inventory.year.isnot(None)).filter(
                           Inventory.fuel== fuel if fuel != 'fuel' else Inventory.fuel.isnot(None)).filter(
                               Inventory.transmission== trans if trans != 'transmission' else Inventory.transmission.isnot(None)).filter(Inventory.state=='available')
        return inventory.order_by(Inventory.created_at.desc()).paginate(page,current_app.config['POSTS_PER_PAGE'],False)

#county
class County(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    SNo = db.Column(db.Integer)
    county = db.Column(db.String(64))

    def __repr__(self):
        return '<county {}>'.format(self.id)

    def Load_Data(file_name):
        data = genfromtxt(file_name,dtype={'names':('SNo','county'),'formats':('i4','U15')},delimiter=',', skip_header=1)
        return data.tolist()

#county
class Vehicle(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    make = db.Column(db.String(64))
    model = db.Column(db.String(64))
    inventories = db.relationship('Inventory', backref='vehicle', lazy='dynamic')
    enquiries = db.relationship('Enquiry', backref='vehicle', lazy='dynamic')
    imports = db.relationship('Import', backref='vehicle', lazy='dynamic')
    sellcars = db.relationship('Sellcar', backref='vehicle', lazy='dynamic')
    tradein = db.relationship('Tradein', backref='vehicle', lazy='dynamic')
    importcar = db.relationship('Importcar', backref='vehicle', lazy='dynamic')

    def __repr__(self):
        return '<Vehicle {}>'.format(self.id)

    def Load_Vehicle_Data(file_name):
        data = genfromtxt(file_name,dtype={'names':('make','model'),'formats':('U30','U30')},delimiter=',', skip_header=1)
        return data.tolist()

    # def to_table_dict(self):
    #     data = [self.make,self.model,self.year]
    #     return data

#year
class Year(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    year = db.Column(db.Integer)

    def __repr__(self):
        return '<Year {}>'.format(self.id)

    def Load_Year_Data(file_name):
        data = genfromtxt(file_name,dtype={'names':('rand','years'),'formats':('i4','i4')},delimiter=',', skip_header=1)
        return data.tolist()

#clients
class Client(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    client_name = db.Column(db.String(64))
    client_type = db.Column(db.String(64))
    phone_number = db.Column(db.Integer, index=True)
    email = db.Column(db.String(64), index=True)
    location = db.Column(db.String(140))
    quotations = db.relationship('Quotation', backref='client', lazy='dynamic')
    enquiries = db.relationship('Enquiry', backref='client', lazy='dynamic')
    imports = db.relationship('Import', backref='client', lazy='dynamic')
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)


    def __repr__(self):
        return '<Client {}>'.format(self.id)

#clients
class Supplier(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    supplier_name = db.Column(db.String(64))
    supplier_type = db.Column(db.String(64))
    phone_number = db.Column(db.Integer, index=True)
    email = db.Column(db.String(64), index=True)
    location = db.Column(db.String(140))
    bank_name       = db.Column(db.String(64))
    bank_country    = db.Column(db.String(64))
    bank_address    = db.Column(db.String(120))
    account_number  = db.Column(db.String(120))
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    bills = db.relationship('Bill', backref='supplier', lazy='dynamic')

    def __repr__(self):
        return '<Client {}>'.format(self.id)

#quotations
class Quotation(db.Model):
    STATUS = OrderedDict([('pending', 'Pending'),('rejected', 'Rejected'),('approved', 'Approved')])
    id = db.Column(db.Integer, primary_key=True)
    quotation_name = db.Column(db.String(64))
    amount = db.Column(db.Integer)
    description = db.Column(db.String(2000))
    quotation_no = db.Column(db.String(140))
    client_id = db.Column(db.Integer, db.ForeignKey('client.id'))
    status = db.Column(db.Enum(*STATUS, name='status_types', native_enum=False), index=True, nullable=True, server_default='pending')
    quotation_date = db.Column(db.DateTime, index=True)
    invoice = db.relationship('Invoice', backref='quotation', uselist=False, cascade='all,delete-orphan')
    products = db.relationship('Product', backref='quotation', lazy='dynamic', cascade='all,delete-orphan')
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)

    def __repr__(self):
        return '<Quotation {}>'.format(self.id)


#invoice
class Invoice(db.Model):
    STATUS = OrderedDict([('pending', 'Pending'),('rejected', 'Rejected'),('approved', 'Approved')])
    id = db.Column(db.Integer, primary_key=True)
    invoice_no = db.Column(db.String(64))
    date_issued = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    date_due = db.Column(db.DateTime)
    status = db.Column(db.Enum(*STATUS, name='status_types', native_enum=False), index=True, nullable=True, server_default='pending')
    quotation_id=db.Column(db.Integer, db.ForeignKey('quotation.id'))
    payment = db.relationship('Payment', backref='invoice', lazy='dynamic', cascade='all,delete-orphan')
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)

    def __repr__(self):
        return '<Invoice {}>'.format(self.id)

#payment
class Payment(db.Model):
    STATUS  = OrderedDict([('pending', 'Pending'),('rejected', 'Rejected'),('approved', 'Approved')])
    MODE  = OrderedDict([('cash', 'Cash'),('cheque', 'Cheque'),('online-payment', 'Online-Payment')])
    id = db.Column(db.Integer, primary_key=True)
    note = db.Column(db.String(2000))
    amount = db.Column(db.Integer)
    invoice_id = db.Column(db.Integer, db.ForeignKey('invoice.id'))
    status = db.Column(db.Enum(*STATUS, name='status_types', native_enum=False), index=True, nullable=True, server_default='pending')
    payment_date = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    payment_mode =db.Column(db.Enum(*MODE, name='payment_mode', native_enum=False), index=True, nullable=True, server_default='cash')
    payment_type = db.Column(db.String(64))
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)

    def __repr__(self):
        return '<Bill {}>'.format(self.id)

#bills
class Bill(db.Model):
    STATUS  = OrderedDict([('pending', 'Pending'),('rejected', 'Rejected'),('approved', 'Approved')])
    id = db.Column(db.Integer, primary_key=True)
    bill_name = db.Column(db.String(64))
    bill_no = db.Column(db.String(64))
    amount = db.Column(db.Integer)
    description = db.Column(db.String(2000))
    supplier_id = db.Column(db.Integer, db.ForeignKey('supplier.id'))
    status = db.Column(db.Enum(*STATUS, name='status_types', native_enum=False), index=True, nullable=True, server_default='pending')
    date_issued = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    date_due = db.Column(db.DateTime, index=True)
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)

    def __repr__(self):
        return '<Bill {}>'.format(self.id)


#product
class Product(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    product = db.Column(db.String(64))
    size = db.Column(db.String(64))
    quantity = db.Column(db.Integer)
    price = db.Column(db.Integer)
    total = db.Column(db.Integer)
    quotation_id=db.Column(db.Integer, db.ForeignKey('quotation.id'))


    def __repr__(self):
        return '<Product {}>'.format(self.id)

#enquiries
class Enquiry(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    budget = db.Column(db.Numeric)
    year = db.Column(db.Integer)
    client_id = db.Column(db.Integer, db.ForeignKey('client.id'))
    vehicle_id = db.Column(db.Integer, db.ForeignKey('vehicle.id'))
    created_by = db.Column(db.Integer, db.ForeignKey('user.id'))
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)

    def __repr__(self):
        return '<Enquiry {}>'.format(self.id)

    def from_enquiry_dict(self, data):
        for field in ['model', 'budget', 'client','year']:
            if field in data:
                if field =='model':
                    id=data[field]
                    field='vehicle'
                    vehicle=Vehicle.query.get_or_404(id)
                    setattr(self, field, vehicle)
                elif field =='client':
                    print('here')
                    id=data[field]
                    field='client'
                    client=Client.query.get_or_404(id)
                    setattr(self, field, client)
                else:
                    setattr(self, field, data[field])



#imports
class Import(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    budget = db.Column(db.Numeric)
    transmission = db.Column(db.String(64))
    color = db.Column(db.String(64))
    fuel = db.Column(db.String(64))
    engine= db.Column(db.String(64))
    year = db.Column(db.Integer)
    special_features= db.Column(db.String(2000))
    client_id = db.Column(db.Integer, db.ForeignKey('client.id'))
    vehicle_id = db.Column(db.Integer, db.ForeignKey('vehicle.id'))
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)

    def __repr__(self):
        return '<Import {}>'.format(self.id)

#sell
class Sellcar(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    phone_number = db.Column(db.String(64))
    email = db.Column(db.String(64), index=True)
    year = db.Column(db.Integer)
    vehicle_id = db.Column(db.Integer, db.ForeignKey('vehicle.id'))
    description = db.Column(db.String(2000))
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)

    def __repr__(self):
        return '<Sellcar {}>'.format(self.id)

#import
class Importcar(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    phone_number = db.Column(db.String(64))
    email = db.Column(db.String(64), index=True)
    budget = db.Column(db.Numeric)
    year = db.Column(db.Integer)
    vehicle_id = db.Column(db.Integer, db.ForeignKey('vehicle.id'))
    description = db.Column(db.String(2000))
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)

    def __repr__(self):
        return '<Importcar {}>'.format(self.id)

#contact us
class Contact(PaginatedAPIEnquiriesMixin,db.Model):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(64))
    last_name = db.Column(db.String(64))
    phone_number = db.Column(db.String(64))
    email = db.Column(db.String(64), index=True)
    subscribe = db.Column(db.Boolean,default=False)
    description = db.Column(db.String(2000))
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)

    def __repr__(self):
        return '<Contact {}>'.format(self.id)

    def to_enquiry_dict(self):
        data = {
            'id': self.id,
            'first_name':self.first_name,
            'last_name': self.last_name,
            'phone_number': self.phone_number,
            'email': self.email,
            'subscribe': self.subscribe,
            'description': self.description,
            'enquiry_date':self.created_at,
            '_links': {
                'self': url_for('api.get_enquiry', id=self.id)
            }
        }
        return data



#sell
class Tradein(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    phone_number = db.Column(db.String(64))
    transmission = db.Column(db.String(64))
    mileage = db.Column(db.String(64))
    year = db.Column(db.Integer)
    vin = db.Column(db.String(64))
    image = db.Column(db.String(64))
    exterior_color = db.Column(db.String(64))
    interior_color = db.Column(db.String(64))
    exterior_condition = db.Column(db.String(64))
    interior_condition = db.Column(db.String(64))
    accident = db.Column(db.String(64))
    email = db.Column(db.String(64), index=True)
    vehicle_id = db.Column(db.Integer, db.ForeignKey('vehicle.id'))
    comments = db.Column(db.String(2000))
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)

    def __repr__(self):
        return '<Tradein {}>'.format(self.id)
