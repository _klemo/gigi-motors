from datetime import datetime, timedelta
from hashlib import md5
from time import time
from flask import current_app, url_for
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from collections import OrderedDict
import jwt
import json
from app import db, login
import redis
import rq
import base64
import os,random
from xkcdpass import xkcd_password as xp

class PaginatedAPIMixin(object):
    @staticmethod
    def to_collection_dict(query, page, per_page, endpoint, **kwargs):
        resources = query.paginate(page, per_page, False)
        data = {
            'items': [item.to_dict() for item in resources.items],
            '_meta': {
                'page': page,
                'per_page': per_page,
                'total_pages': resources.pages,
                'total_items': resources.total
            },
            '_links': {
                'self': url_for(endpoint, page=page, per_page=per_page,
                                **kwargs)
            }
        }
        return data

#roles
class Role(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    role = db.Column(db.String(64))
    access_inventory = db.Column(db.Boolean, default=False)
    access_user = db.Column(db.Boolean, default=False)
    permissions = db.relationship('Permission', backref='role', uselist=False, cascade='all,delete-orphan')
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    user = db.relationship('User', backref='role', cascade='all,delete-orphan')

    def __repr__(self):
        return '<Role {}>'.format(self.id)

    def to_dict_role(self):
        data = {
            'id': self.id,
            'access_inventory': self.access_inventory,
            'access_user': self.access_user,
            'read_inventory': self.permissions.read_inventory,
            'read_user': self.permissions.read_user,
            'write_inventory': self.permissions.write_inventory,
            'write_user': self.permissions.write_user,
            'create_inventory': self.permissions.create_inventory,
            'create_user': self.permissions.create_user,
            'delete_inventory': self.permissions.delete_inventory,
            'delete_user': self.permissions.delete_user
        }
        return data

    def from_dict(self, data):
        for field in ['access_inventory','access_user']:
            if field in data:
                if data[field] == 'false':
                    setattr(self, field, False)
                elif data[field] == 'true':
                    setattr(self, field, True)

    def from_dict_first_registration(self):
        for field in ['access_inventory','access_user']:
            setattr(self, field, True)

#user login
class User(PaginatedAPIMixin,UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    status = db.Column(db.Boolean, default=True)
    role_id=db.Column(db.Integer, db.ForeignKey('role.id'))
    phone_number = db.Column(db.String(120))
    last_seen = db.Column(db.DateTime, default=datetime.utcnow)
    token = db.Column(db.String(32), index=True, unique=True)
    token_expiration = db.Column(db.DateTime)
    reserved = db.relationship('Inventory', backref='reserver', lazy='dynamic')
    enquiries = db.relationship('Enquiry', backref='author', lazy='dynamic')

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def get_reset_password_token(self, expires_in=600):
        return jwt.encode(
            {'reset_password': self.id, 'exp': time() + expires_in},
            current_app.config['SECRET_KEY'],
            algorithm='HS256').decode('utf-8')

    @staticmethod
    def verify_reset_password_token(token):
        try:
            id = jwt.decode(token, current_app.config['SECRET_KEY'],
                            algorithms=['HS256'])['reset_password']
        except:
            return
        return User.query.get(id)

    def get_token(self, expires_in=3600):
        now = datetime.utcnow()
        if self.token and self.token_expiration > now + timedelta(seconds=60):
            return self.token
        self.token = base64.b64encode(os.urandom(24)).decode('utf-8')
        self.token_expiration = now + timedelta(seconds=expires_in)
        db.session.add(self)
        return self.token

    def revoke_token(self):
        self.token_expiration = datetime.utcnow() - timedelta(seconds=1)

    @staticmethod
    def check_token(token):
        user = User.query.filter_by(token=token).first()
        if user is None or user.token_expiration < datetime.utcnow():
            return None
        return user

    def capitalize_first_letter(self,s):
        new_str = []
        s = s.split(" ")
        for i, c in enumerate(s):
            new_str.append(c.capitalize())
        return "".join(new_str)

    def to_dict(self, include_email=False):
        data = {
            'id': self.id,
            'username': self.username,
            'email': self.email,
            'role':self.role.role,
            'phone_number':self.phone_number,
            'last_seen': self.last_seen.isoformat() + 'Z',
            '_links': {
                'self': url_for('api.get_user', id=self.id)
            }
        }
        if include_email:
            data['email'] = self.email
        return data

    def from_dict(self, data, new_user=False):
        for field in ['username', 'email', 'about_me']:
            if field in data:
                setattr(self, field, data[field])
        if new_user and 'password' in data:
            self.set_password(data['password'])


@login.user_loader
def load_user(id):
    return User.query.get(int(id))

#permissions
class Permission(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    read_inventory = db.Column(db.Boolean, default=False)
    read_user = db.Column(db.Boolean, default=False)
    write_inventory = db.Column(db.Boolean, default=False)
    write_user = db.Column(db.Boolean, default=False)
    create_inventory = db.Column(db.Boolean, default=False)
    create_user = db.Column(db.Boolean, default=False)
    delete_inventory = db.Column(db.Boolean, default=False)
    delete_user = db.Column(db.Boolean, default=False)
    role_id = db.Column(db.Integer, db.ForeignKey('role.id'))
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)

    def __repr__(self):
        return '<Role {}>'.format(self.id)

    def from_dict_permissions(self, data):
        for field in ['read_inventory','read_user','write_inventory','write_user','create_inventory','create_user','delete_inventory','delete_user']:
            if field in data:
                if data[field] == 'false':
                    setattr(self, field, False)
                elif data[field] == 'true':
                    setattr(self, field, True)

    def from_dict_permissions_first_registration(self):
        for field in ['read_inventory','read_user','write_inventory','write_user','create_inventory','create_user','delete_inventory','delete_user']:
            setattr(self, field, True)

#company settings
class Company(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    company_name    = db.Column(db.String(64))
    contact_person  = db.Column(db.String(64))
    address         = db.Column(db.String(120))
    country         = db.Column(db.String(64))
    city            = db.Column(db.String(64))
    state           = db.Column(db.String(64))
    postal_code     = db.Column(db.String(64))
    email           = db.Column(db.String(64), index=True)
    phone_number    = db.Column(db.Integer, index=True)
    bank_name       = db.Column(db.String(64))
    bank_country    = db.Column(db.String(64))
    bank_address    = db.Column(db.String(120))
    account_number  = db.Column(db.String(120))

    def __repr__(self):
        return '<Company {}>'.format(self.id)

#theme settings
class Theme(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    company_name    = db.Column(db.String(64))
    logo            = db.Column(db.String(64))
    favicon         = db.Column(db.String(120))

    def __repr__(self):
        return '<Theme {}>'.format(self.id)
