Dear {{ user.username }},

To reset your password click on the following link:

Your login Credentials are:
email:{{ user.email }}
password:{{password}}

To login click on the following link:

{{ url_for('auth.login', _external=True) }}

If you have not been added as a tenant please ignore this message.

Sincerely,

The Lescho Team
