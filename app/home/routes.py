from datetime import datetime
from flask import render_template, flash, redirect, abort, url_for, request, g, jsonify, current_app
from flask_login import current_user, login_required
from flask_uploads import UploadSet, configure_uploads, IMAGES
from app import db
from flask import Blueprint
from app.models import *
from app.home import bp
from app.home.forms import *
from sqlalchemy import and_
photos = UploadSet('photos', IMAGES)

@bp.before_app_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()
    g.company = Company.query.first()
    g.images = Theme.query.first()


@bp.route('/')
@bp.route('/index')
def index():
    form=SearchForm()
    make=[(row.make, row.make) for row in Vehicle.query.group_by(Vehicle.make)]
    make.insert(0, ('make','--Any make--'))
    year=[(row.year, row.year) for row in Year.query.order_by(Year.year.desc()).all()]
    year.insert(0, (0,'--Any year--'))
    model=[(row.model, row.model) for row in Vehicle.query.group_by(Vehicle.model)]
    model.insert(0, ('model','--Any model--'))
    form.year.choices=year
    form.make.choices=make
    form.model.choices=model
    featured = Inventory.query.filter_by(state='available',featured=True).order_by(Inventory.created_at.desc()).limit(4)
    return render_template('home/index.html',form=form, featured=featured)

@bp.route('/contact_us', methods=['GET', 'POST'])
def contact_us():
    form=ContactForm()
    if form.validate_on_submit():
        contact = Contact(first_name=form.first_name.data,last_name=form.last_name.data, phone_number=form.phone_number.data,email=form.email.data,description=form.description.data,subscribe=form.subscribe.data)
        db.session.add(contact)
        db.session.commit()
        flash('Request sent successfully! We will get back to you as soon as possible ', 'success')
        return redirect(url_for('home.contact_us'))
    return render_template('home/contactus.html',form=form)

@bp.route('/financing')
def financing():
    return render_template('home/financing.html')

@bp.route('/imports', methods=['GET', 'POST'])
def imports():
    form=ImportcarForm()
    make=[(row.make, row.make) for row in Vehicle.query.group_by(Vehicle.make)]
    make.insert(0, (0,'--Any make--'))
    year=[(row.year, row.year) for row in Year.query.order_by(Year.year.desc()).all()]
    year.insert(0, (0,'--choose year--'))
    model=[(row.model, row.model) for row in Vehicle.query.group_by(Vehicle.model)]
    model.insert(0, (0,'--choose model--'))
    form.year.choices=year
    form.make.choices=make
    form.model.choices=model
    if form.validate_on_submit():
        model= Vehicle.query.filter_by(make=form.make.data,model=form.model.data).first_or_404()
        car = Importcar(vehicle=model,year=form.year.data,name=form.name.data,budget=form.budget.data, phone_number=form.phone_number.data,email=form.email.data,description=form.description.data)
        db.session.add(car)
        db.session.commit()
        flash('Request sent successfully! We will get back to you as soon as possible ', 'success')
        return redirect(url_for('home.imports'))
    return render_template('home/import.html',form=form)

@bp.route('/inventory')
def inventory():
    form=SearchForm()
    make=[(row.make, row.make) for row in Vehicle.query.group_by(Vehicle.make)]
    make.insert(0, ('make','--Any make--'))
    year=[(row.year, row.year) for row in Year.query.order_by(Year.year.desc()).all()]
    year.insert(0, (0,'--Any year--'))
    model=[(row.model, row.model) for row in Vehicle.query.group_by(Vehicle.model)]
    model.insert(0, ('model','--Any model--'))
    form.year.choices=year
    form.make.choices=make
    form.model.choices=model
    page = request.args.get('page', 1, type=int)
    inventory = Inventory.query.filter_by(state='available').order_by(Inventory.created_at.desc()).paginate(page,current_app.config['POSTS_PER_PAGE'],False)
    featured = Inventory.query.filter_by(state='available',featured=True).order_by(Inventory.created_at.desc()).limit(3)
    maxlist=Inventory.query.all()
    maxval =  int(max(node.price for node in maxlist )) if maxlist else  None
    if request.args.get('year') and form.validate():
        page = request.args.get('page', 1, type=int)
        year = form.year.data
        make = form.make.data
        model = form.model.data
        fuel = form.fuel.data
        trans = form.transmission.data
        inventory = Inventory.get_inventory_filters(make,model,year,fuel,trans,page)
    elif request.args.get('body'):
        body = request.args.get('body', '01', type=str)
        inventory =Inventory.query.filter(and_(Inventory.body.like('%' + body.capitalize() + '%'),Inventory.state=='available')).order_by(Inventory.created_at.desc()).paginate(page,current_app.config['POSTS_PER_PAGE'],False)
    next_url = url_for('home.inventory', page=inventory.next_num) if inventory.has_next else None
    prev_url = url_for('home.inventory', page=inventory.prev_num) if inventory.has_prev else None
    return render_template('home/inventory.html', inventories=inventory.items,form=form,featured=featured, next_url=next_url, prev_url=prev_url,maxval=maxval)

@bp.route('/sell_your_car', methods=['GET', 'POST'])
def sell_your_car():
    form=SellcarForm()
    make=[(row.make, row.make) for row in Vehicle.query.group_by(Vehicle.make)]
    make.insert(0, (0,'--choose make--'))
    year=[(row.year, row.year) for row in Year.query.order_by(Year.year.desc()).all()]
    year.insert(0, (0,'--choose year--'))
    model=[(row.model, row.model) for row in Vehicle.query.group_by(Vehicle.model)]
    model.insert(0, (0,'--choose model--'))
    form.year.choices=year
    form.make.choices=make
    form.model.choices=model
    if form.validate_on_submit():
        model= Vehicle.query.filter_by(make=form.make.data,model=form.model.data).first_or_404()
        car = Sellcar(vehicle=model,name=form.name.data,year=form.year.data, phone_number=form.phone_number.data,email=form.email.data,description=form.description.data)
        db.session.add(car)
        db.session.commit()
        flash('Request sent successfully! We will get back to you as soon as possible ', 'success')
        return redirect(url_for('home.sell_your_car'))
    return render_template('home/sellyourcar.html',form=form)

@bp.route('/tips')
def tips():
    return render_template('home/tips.html')

@bp.route('/trade_in', methods=['GET', 'POST'])
def trade_in():
    form=TradeinForm()
    make=[(row.make, row.make) for row in Vehicle.query.group_by(Vehicle.make)]
    make.insert(0, (0,'--choose make--'))
    year=[(row.year, row.year) for row in Year.query.order_by(Year.year.desc()).all()]
    year.insert(0, (0,'--choose year--'))
    model=[(row.model, row.model) for row in Vehicle.query.group_by(Vehicle.model)]
    model.insert(0, (0,'--choose model--'))
    form.year.choices=year
    form.make.choices=make
    form.model.choices=model
    if form.validate_on_submit():
        image = form.image.data
        if image is not None:
            image = photos.save(form.image.data)
        model= Vehicle.query.filter_by(make=form.make.data,model=form.model.data).first_or_404()
        car = Tradein(vehicle=model,name=form.name.data,year=form.year.data, phone_number=form.phone_number.data,email=form.email.data,comments=form.comments.data,transmission=form.transmission.data,
                      mileage=form.mileage.data,vin=form.vin.data,image=image,exterior_color=form.exterior_color.data,interior_color=form.interior_color.data,exterior_condition=form.exterior_condition.data,
                      interior_condition=form.interior_condition.data,accident=form.accident.data)
        db.session.add(car)
        db.session.commit()
        flash('Request sent successfully! We will get back to you as soon as possible ', 'success')
        return redirect(url_for('home.trade_in'))
    return render_template('home/tradein.html',form=form)

@bp.route('/_get_cars/')
def _get_cars():
    min_val = request.args.get('min_price', '01', type=int)
    max_val = request.args.get('max_price', '01', type=int)
    page = request.args.get('page', 1, type=int)
    data={}
    inventory =Inventory.query.filter(Inventory.price >= min_val).filter(Inventory.price <= max_val).filter(Inventory.state=='available').order_by(Inventory.created_at.desc()).paginate(page,current_app.config['POSTS_PER_PAGE'],False)
    next_url = url_for('home.inventory', page=inventory.next_num) if inventory.has_next else None
    prev_url = url_for('home.inventory', page=inventory.prev_num) if inventory.has_prev else None
    #dont delete
    # for index,list in enumerate(inventory):
    #     data.update({index:{
    #                 'price': list.price,
    #                 'mileage': list.mileage,
    #                 'image': list.image,
    #                 'fuel': list.fuel,
    #                 'year': list.vehicle.year,
    #                 'make': list.vehicle.make,
    #                 'model':list.vehicle.model if list else  None}})
    # print(data)
    # return jsonify(data)
    return render_template('home/_post.html', inventories=inventory.items,next_url=next_url, prev_url=prev_url)

@bp.route('/_get_body')
def _get_body():
    data = request.args
    list=Inventory.to_dict_body(data)
    page = request.args.get('page', 1, type=int)
    inventory =Inventory.query.filter_by(state='available').order_by(Inventory.created_at.desc()).paginate(page,current_app.config['POSTS_PER_PAGE'],False)
    if list:
        inventory =Inventory.query.filter(Inventory.body.in_(list)).filter(Inventory.state=='available').order_by(Inventory.created_at.desc()).paginate(page,current_app.config['POSTS_PER_PAGE'],False)
    next_url = url_for('home.inventory', page=inventory.next_num) if inventory.has_next else None
    prev_url = url_for('home.inventory', page=inventory.prev_num) if inventory.has_prev else None
    return render_template('home/_post.html', inventories=inventory.items,next_url=next_url, prev_url=prev_url)


@bp.route('/_filter_by_inventory/')
def _filter_by_inventory():
    page = request.args.get('page', 1, type=int)
    year = request.args.get('year', 0, type=int)
    make = request.args.get('make', '01', type=str)
    model = request.args.get('model', '01', type=str)
    fuel = request.args.get('fuel', '01', type=str)
    trans = request.args.get('trans', '01', type=str)
    inventory = Inventory.get_inventory_filters(make,model,year,fuel,trans,page)
    next_url = url_for('home.inventory', page=inventory.next_num) if inventory.has_next else None
    prev_url = url_for('home.inventory', page=inventory.prev_num) if inventory.has_prev else None
    return render_template('home/_post.html', inventories=inventory.items,next_url=next_url, prev_url=prev_url)


@bp.route('/body_type/<body>')
def body_type(body):
    form=SearchForm()
    make=[(row.make, row.make) for row in Vehicle.query.group_by(Vehicle.make)]
    make.insert(0, ('make','--Any make--'))
    year=[(row.year, row.year) for row in Year.query.order_by(Year.year.desc()).all()]
    year.insert(0, (0,'--Any year--'))
    model=[(row.model, row.model) for row in Vehicle.query.group_by(Vehicle.model)]
    model.insert(0, ('model','--Any model--'))
    form.year.choices=year
    form.make.choices=make
    form.model.choices=model
    page = request.args.get('page', 1, type=int)
    inventory =Inventory.query.filter(and_(Inventory.body.like('%' + body.capitalize() + '%'),Inventory.state=='available')).order_by(Inventory.created_at.desc()).paginate(page,current_app.config['POSTS_PER_PAGE'],False)
    maxlist=Inventory.query.all()
    maxval =  int(max(node.price for node in maxlist )) if maxlist else  None
    featured = Inventory.query.filter_by(state='available',featured=True).order_by(Inventory.created_at.desc()).limit(3)
    next_url = url_for('home.body_type',body=body, page=inventory.next_num) if inventory.has_next else None
    prev_url = url_for('home.body_type',body=body, page=inventory.prev_num) if inventory.has_prev else None
    return render_template('home/inventory.html',form=form,featured=featured, inventories=inventory.items,next_url=next_url, prev_url=prev_url,maxval=maxval)

@bp.route('/_get_model/')
def _get_model():
    make = request.args.get('make', '01', type=str)
    model =[(row.model, row.model) for row in Vehicle.query.filter_by(make=make).group_by(Vehicle.model)]
    model.insert(0, ('model','--Any model--'))
    return jsonify(model)
