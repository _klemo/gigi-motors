from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, SelectField, TextAreaField,IntegerField,RadioField, DecimalField
from wtforms.validators import DataRequired, Email, EqualTo, Length
from wtforms import ValidationError, validators
from flask_babel import lazy_gettext as _l
from app.models.user import User
from flask import request
from flask_uploads import UploadSet, configure_uploads, IMAGES
from flask_wtf.file import FileField, FileRequired, FileAllowed
from flask_wtf.recaptcha import RecaptchaField
photos = UploadSet('photos', IMAGES)

class SearchForm(FlaskForm):
    year = SelectField(coerce=int,id="car-years")
    make = SelectField(id="car-makes")
    model =SelectField(id="car-models")
    transmission =SelectField(choices=[('transmission', '--Transmission Type--'),('Automatic', 'Automatic'), ('Manual', 'Manual')],id="car-trans")
    fuel =SelectField(choices=[('fuel', '--Fuel Type--'),('Petrol', 'Petrol'), ('Diesel', 'diesel')],id="car-fuel")
    submit = SubmitField('FIND A CAR')

    def __init__(self, *args, **kwargs):
        if 'formdata' not in kwargs:
            kwargs['formdata'] = request.args
        if 'csrf_enabled' not in kwargs:
            kwargs['csrf_enabled'] = False
        super(SearchForm, self).__init__(*args, **kwargs)

class SellcarForm(FlaskForm):
    year = SelectField(validators=[DataRequired()],coerce=int,id="car-years")
    make = SelectField(validators=[DataRequired()],id="car-makes")
    model =SelectField(validators=[DataRequired()],id="car-models")
    name = StringField(validators=[DataRequired()])
    phone_number = StringField(validators=[DataRequired()])
    email = StringField('email', validators=[DataRequired(), Email()])
    description = TextAreaField(validators=[DataRequired()])
    submit = SubmitField('SUBMIT')

class ContactForm(FlaskForm):
    first_name = StringField(validators=[DataRequired()])
    last_name  = StringField(validators=[DataRequired()])
    phone_number = StringField(validators=[DataRequired()])
    email = StringField('email', validators=[DataRequired(), Email()])
    description = TextAreaField(validators=[Length(min=20, max=300)])
    subscribe = BooleanField('Subscribe and Get latest updates and offers by Email')
    recaptcha = RecaptchaField()
    submit = SubmitField('SUBMIT')

class ImportcarForm(FlaskForm):
    year = SelectField(validators=[DataRequired()],coerce=int,id="car-years")
    make = SelectField(validators=[DataRequired()],id="car-makes")
    model =SelectField(validators=[DataRequired()],id="car-models")
    name = StringField(validators=[DataRequired()])
    budget = DecimalField(validators=[DataRequired()])
    phone_number = StringField(validators=[DataRequired()])
    email = StringField('email', validators=[DataRequired(), Email()])
    description = TextAreaField(validators=[DataRequired()])
    submit = SubmitField('SUBMIT')

class TradeinForm(FlaskForm):
    year = SelectField(validators=[DataRequired()],coerce=int,id="car-years")
    make = SelectField(validators=[DataRequired()],id="car-makes")
    model =SelectField(validators=[DataRequired()],id="car-models")
    transmission =SelectField(validators=[DataRequired()],choices=[('automatic', 'Automatic'), ('manual', 'Manual')])
    mileage =StringField(validators=[DataRequired()])
    vin = StringField(validators=[DataRequired()])
    image = FileField(validators=[FileAllowed(photos, u'Image only!')])
    exterior_color = StringField(validators=[DataRequired()])
    interior_color = StringField(validators=[DataRequired()])
    exterior_condition = RadioField(validators=[DataRequired()],choices=[('extra-clean','Extra clean'),('clean','Clean'),('average','Average'),('below-average','Below Average'),('dont-know',"I don't know")])
    interior_condition = RadioField(validators=[DataRequired()],choices=[('extra-clean','Extra clean'),('clean','Clean'),('average','Average'),('below-average','Below Average'),('dont-know',"I don't know")])
    accident = RadioField(validators=[DataRequired()],choices=[('yes','Yes'),('no','NO'),('dont-know',"I don't know")])
    name = StringField(validators=[DataRequired()])
    phone_number = StringField(validators=[DataRequired()])
    email = StringField('email', validators=[DataRequired(), Email()])
    comments = TextAreaField(validators=[DataRequired()])
    submit = SubmitField('SUBMIT')
